import {useState, useEffect} from 'react';
import {useHistory, useParams} from 'react-router-dom'
import {Container} from './style'

import {apikey, timeStamp} from '../../services/api';
import api from '../../services/api';

export const Details = () => {
  const {id} = useParams();
  const [data, setData ] = useState([]);

  const history = useHistory()

  useEffect(() => {
    const getDetailsOfCharacter = async() => {
      try{
        const response = await api.get(`characters/${id}?&ts=${timeStamp}&apikey=${apikey}&hash=5c68ac76c29e4cde041dcb1c05cdd411`)
        setData(response.data.data.results)
        console.log(response.data.data.results)
      }catch(err) {
        console.log(err, 'Não foi')
      }

    }
    getDetailsOfCharacter()
  }, [])


  return (
    <Container>
      {data.map(info => (
        <>
        <section className="first-section">
            <div className="avatar">
              <img src={`${info.thumbnail.path}.${info.thumbnail.extension}`}
              alt="Avatar"/>
            </div>

          <form>
            <input type="text" value={info.name}/>
            <textarea rows="5" cols="25">{info.description || 'No Description'}</textarea>
          </form>

          <div className="btn-back">
            <button type="button" onClick={history.goBack}>
              Voltar
            </button>
          </div>
        </section>


      <h2>Fascículos</h2>
      <div className="divider" />

        <section className="second-section">
          <div className="card">

            <div className="fascicle-info">
              <p>Título: h1</p>
              <p>número de capa: 3</p>
            </div>


            <div className="fascicle-description">
              <img src={`${info.thumbnail.path}.${info.thumbnail.extension}`} alt="Avatar"/>
              <textarea row="100" cols="100">Descrição</textarea>
            </div>

          </div>
          <div className="divider" />
        </section>
        </>
      ))}

    </Container>
  )
}
