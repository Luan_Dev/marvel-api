import styled from 'styled-components';

export const Container = styled.main`
  max-width: 1200px;
  width: 90%;
  height: 100vh;
  margin: 5rem auto;

  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: column;


  .first-section {
    width: 90%;
    display: flex;
    align-items: center;
    justify-content: space-around;
    margin: 1rem;

  .avatar img {
    width: 8rem;
    height: 8rem;
  }

  form {
    width: 24rem;
    display: flex;
    flex-direction: column;

    input {
      margin: 1rem 0;
      padding: 0.5rem;
    }

    textarea {
      padding: 0.5rem;
    }
  }

 button {
  width: 10rem;
  padding: 1rem;
  text-align: center;
  margin-top: 8.5rem;

  border: 0;
  border-radius: 1rem;
  background: var(--blue);

  color: var(--white);
  font-size: 1rem;
  font-weight: normal;
  letter-spacing: 0.1rem;
  transition: 0.2s;

  &:hover {
    filter: brightness(0.8);
  }
}
  }

  .divider {
  width: 80%;
  border: 0.5px solid #666;
  }

  .second-section {
    width: 100%;
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 1rem;

    .card {
      width: 80%;
      margin: 0 auto;


      .fascicle-info {
        display: flex;
        align-items: center;
        justify-content: space-around;

        p {
          font-weight: bold;
        }

      }

    }

    .fascicle-description{
      width: 100%;

      img {
        width: 5rem;
        height: 6rem;
        margin-right: 1rem;
      }

      textarea {
        height: 5rem;
        width: 80%;
      }
    }
  }


`;
