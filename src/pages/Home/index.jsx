import {useState, useEffect} from 'react';
import { Posts } from '../../components/Posts';
import { Pagination } from '../../components/Pagination';
import {timeStamp, apikey, hash} from '../../services/api'
import api from '../../services/api';

import {Container} from './style';

export const Home = () => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(15);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  useEffect(() => {
    const getData = async() => {
      const response = await api.get(`characters?ts=${timeStamp}&apikey=${apikey}&hash=5c68ac76c29e4cde041dcb1c05cdd411`)
      setPosts(response.data.data.results)
    }
    getData()
  }, [])

  return (
   <Container>
     <Posts posts={currentPosts} loading={loading}/>
     <Pagination
      postsPerPage={postsPerPage}
      totalPosts={posts.length}
      paginate={paginate}
     />
   </Container>
  )
}
