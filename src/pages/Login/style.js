import styled from 'styled-components';

export const Container = styled.main`
  @media(min-width: 320px) {
    width: 95%;
    height: 100vh;
    margin: 0 auto;

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    h2 {
      font-size: 2rem;
    }

    form {
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;

      input {
        width: 20rem;
        padding: 1rem;
        margin:  0.5rem 0;

        border: 1px solid var(--text-body);
      }

      button {
        width: 20rem;
        padding: 1rem;
        margin-top: 0.4rem;

        border: 0;
        border-radius: 0.2rem;
        background: var(--blue);

        color: var(--white);
        font-size: 1rem;
        font-weight: bold;
        letter-spacing: 0.1rem;

      }
    }
  }

  @media(min-width: 720px) {
    form {
      input {
       width: 45rem;
      }

      button {
        width: 15rem;
      }
    }
  }
`
