import {useState} from 'react';
import {Container} from './style';
import {useForm} from 'react-hook-form'
import {timeStamp} from '../../services/api'
import api from '../../services/api';
import {Home} from '../Home'

import md5 from 'md5';
import { Link } from 'react-router-dom';

export const apikey = 'fd68ed2553e769eceef07a2e8a2bdcc4'

export const Login = () => {
  const {register, handleSubmit, watch, formState: {errors}} = useForm();
  const [isLoading, setIsLoading] = useState(false);
  const [isLogged, setIsLogged] = useState(false);

  const onSubmit = async(data) => {
    const hash = md5(timeStamp+data.private_key+data.public_key)

    try {
      setIsLoading(true)
      const response = await api.get(`characters?ts=${timeStamp}&apikey=${apikey}&hash=${hash}`)
      setIsLogged(true);
      if(response.status === 200) {
        window.location = "/home"
      }

    }catch(err) {
      console.log('Ainda não')
    }finally {
      setIsLoading(false);
    }
  };

  function handleClick () {
    if(!isLogged) {
      return <Link to="/home"/>
    }
  }


  return (
      <Container>
      <h2>Dados de acesso</h2>

      <form onSubmit={handleSubmit(onSubmit)}>
        <input
        required
        type="text"
        placeholder="public_key"
        {...register('public_key', {required: true, maxLength: 32})}
        />

        <input
        type="text"
        placeholder="private_key"
        {...register('private_key',  {required: true, maxLength: 40})}
        />
        {errors.private_key && "private_key error, please try again"}


        <button type="submit">
          Acessar
        </button>
      </form>
    </Container>

  )
}
