import {call, put, takeEvery, select} from 'redux-saga/effects';
import {
  LOAD_ALL_CHARACTERS,
  FILTER_CHARACTERS,
  LOAD_CHARACTER,
  LOAD_CHARACTER_COMICS,
} from '../types';

import api from '../../services/api';

export const getState = (state) => state.app;

