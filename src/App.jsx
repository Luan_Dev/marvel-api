import {GlobalStyle} from './styles/global.js';
import {Login} from './pages/Login';
import {Home} from './pages/Home';
import {Details} from './pages/Details';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

export const App = () => {

  return (
    <>
      <Router>
         <Route exact path="/" component={Login} />
         <Route path="/home" component={Home}/>
         <Route path="/details/:id" component={Details}/>
      </Router>
      <GlobalStyle />
    </>
  )
}
