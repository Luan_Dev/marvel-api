import axios from 'axios';

export const apikey = 'fd68ed2553e769eceef07a2e8a2bdcc4'
export const timeStamp = '1619295482';

const api = axios.create({
  baseURL: `https://gateway.marvel.com:443/v1/public/`
});

export default api;
