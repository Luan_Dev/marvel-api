import styled from 'styled-components';

export const Container = styled.table `
  border-collapse: collapse;
  width: 95%;
  margin: 0 auto;

  td, th {
    border: 2px solid var(--text-body);
    text-align: left;
    padding: 0.5rem;
    a {
      text-decoration: none;
      color: inherit;
    }
  }
`
