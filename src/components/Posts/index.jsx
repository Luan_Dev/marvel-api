import React, { useState } from 'react';
import { Link } from 'react-router-dom'
import {Container } from './style';

export const Posts = ({posts, loading}) => {
  if(loading) {
    return <h2>loading...</h2>
  }

  return (
    <Container>
    <tr>
      <th>Nome</th>
      <th>Descrição</th>
      <th>última atualização</th>
    </tr>
      {posts.map(item => (
      <tr key={item.id}>
        <td>
          <Link to={`/details/${item.id}`}>{item.name}</Link>
        </td>
        <td>{item.description}</td>
        <td>{item.modified}</td>
      </tr>
      ))}
  </Container>
  )
}
