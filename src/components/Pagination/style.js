import styled from 'styled-components';

export const Container = styled.nav`

  ul {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 20px;
    margin-top: 1rem;

    li {
      list-style: none;
      padding: 0 0.5rem;

      a {
        text-decoration: none;
        color: var(--blue);
        font-size: 1.3rem;
        transition: 0.2s;

        &:hover{
         font-size: 1.6rem;
         cursor: pointer;
        }
      }
    }
  }
`
