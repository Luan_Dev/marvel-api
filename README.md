<h1 align="center">Printi Test</h1>

## How to install

- First acess the link [Marvel Api](https://developer.marvel.com/) and get your public_key and private_key.

- Now follow these steps.

```bash
# First do the git clone.
$ git clone git@bitbucket.org:Luan_Dev/marvel-api.git

# Now access the repository in you path and open the source code

#In you code, install all depencies.
$ yarn or npm install

#Then run the project
$ yarn dev

```

---
## If everithing goes well, you must see that page.
- Put your keys that you get on marvel's api site.
<h1>
  <img src="src/assets/acess.png" alt="">
</h1>

